## Documentation

- [Project Assignment](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/blob/master/Project%20Assignment.pdf)

- [Report](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/blob/master/Report.pdf)

- [Slides Presentation](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/blob/master/Slides%20Presentation.pdf)

## Code

- [Exercises 1-2-3-4 Spark](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/tree/Exercises_1234_Spark)
- [Exercises 1-2-4 Spark Streaming](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/tree/exercises_124_SparkStreaming)


- [Wiki Threads](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/tree/Wiki_Threads)
- [Wiki Sparks](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/tree/Wiki_Spark)
- [Wiki Hadoop](https://gitlab.com/paolino625/systems-for-big-data-lab_delivery/-/tree/Wiki_Hadoop)
